import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    axiosHTTP: {
      baseURL: 'http://192.168.0.10:8082',
      token: 'AD9F5B68DB73B5149F2A645997D77891',
    }
  },
  mutations: {

  },
  actions: {

  }
})
