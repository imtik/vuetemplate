// 限制输入最大数字
const maxnumber = (el, binding) => {
  let max = binding.value;
  let maxLen = String(max).length;
  max = Number(max) || 0;
  if (el.tagName.toUpperCase() !== "INPUT") {
    el = el.querySelector("input");
  }
  if (el) {
    // isNaN(Number(e.target.value)) 判断输入的是数字
    el.setAttribute("maxlength", maxLen);
    el.addEventListener("keyup", e => {
      // 判断value是否NaN
      if (isNaN(Number(e.target.value))) e.target.value = 0;
      if (e.target.value) {
        e.target.value = String(e.target.value).replace(/[^\d]/g, "");
        e.target.value = parseInt(e.target.value);
        if (max > 0) {
          if (e.target.value >= max) e.target.value = max;
        }
      }
    });
    el.addEventListener("input", e => {
      // 判断value是否NaN
      if (isNaN(Number(e.target.value))) e.target.value = 0;
      if (e.target.value) {
        e.target.value = String(e.target.value).replace(/[^\d]/g, "");
        e.target.value = parseInt(e.target.value);
        if (max > 0) {
          if (e.target.value >= max) e.target.value = max;
        }
      }
    });
    el.addEventListener("change", e => {
      // 判断value是否NaN
      if (isNaN(Number(e.target.value))) e.target.value = 0;
      if (e.target.value) {
        e.target.value = String(e.target.value).replace(/[^\d]/g, "");
        e.target.value = parseInt(e.target.value);
        if (max > 0) {
          if (e.target.value >= max) e.target.value = max;
        }
      }
    });
  }
};

// 元素全屏
const fullScreen = (el, bind) => {
  let screenHeight = document.documentElement.clientHeight;
  el.style.height = screenHeight + 'px';
  window.onresize = () => {
    screenHeight = document.documentElement.clientHeight;
    el.style.height = screenHeight + 'px';
  }
}

// 多行文字省略
const ellipsis = (el, bind) => {
  let text = el.innerHTML.trim();
  let textLen = text.length;
  let max = bind.value;
  if (textLen > max) {
    el.innerHTML = text.substr(0, max) + '...';
  }
}

export default [
  maxnumber,
  fullScreen,
  ellipsis
]