import Pagination from './components/pagination'

const allUnit = {
  install: function (Vue) {
    Vue.component("pagination", Pagination);
  }
}

export default allUnit