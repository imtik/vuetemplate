// 格式化时间戳
const formDate = (value, hasTime) => {
  if (!value) return "";
  if (value.toString().length !== 13) {
    value = value * 1000;
  }
  const date = new Date(value);
  let result = "";
  let Y = date.getFullYear() + "-";
  let M =
    (date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1) + "-";
  let D = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
  result = Y + M + D;
  if (hasTime) {
    let h =
      (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":";
    let m =
      (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes()) +
      ":";
    let s =
      date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
    result += " " + h + m + s;
  }
  return result;
};

export default [
  formDate,
]