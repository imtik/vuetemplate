const mixin = {
  data() {
    return {

    }
  },
  computed: {

  },
  methods: {
    go(path, params, type = "path") {
      if (type == "name") {
        this.$router.push({
          name: path,
          params
        });
      } else {
        this.$router.push({
          path,
          query: params
        });
      }
    },
    findEl(el, name) {
      let target = el;
      let targetName = target.getAttribute("name");
      if (name) {
        // 向上寻找有指定name属性的祖先元素
        while (!targetName || targetName != name) {
          if (target.tagName == "BODY") {
            return "err";
          }
          target = target.parentNode;
          targetName = target.getAttribute("name");
        }
      } else {
        // 向上寻找第一个有name属性的祖先元素
        while (!targetName) {
          target = target.parentNode;
          targetName = target.getAttribute("name");
        }
      }
      return target;
    },
    hasClass(el, className) {
      try {
        if (!className) throw "不存在";
        if (typeof className !== "string") throw "类型错误";
        let cls = el.className.split(" ");

        let result = false;

        cls.forEach(v => {
          if (v === className) result = true;
        });
        return result;
      } catch (err) {
        alert ("关键参数 className:", err);
      }
    },
  }
}

export default mixin;