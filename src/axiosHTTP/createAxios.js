// axios
import axios from 'axios'
// import qs from 'qs'
import store from '@/vuex/store'
// import router from '@/router/router'

axios.defaults.baseURL = store.state.axiosHTTP.baseURL;
// 请求头设置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';

/**
 * 
 * @param {String} methods     方法
 * @param {String} url         地址 
 * @param {Object} params      数据
 * @param {Number | String} id id参数
 * @param {Object} userConfig  自定义设置
 * 先创建实例之后在请求拦截器判断请求类型
 */
function createAxios (method, url, params = '', id = null, userConfig = null) {

  // 创建实例
  const axiosInstance = axios.create();

  // 请求拦截器
  axiosInstance.interceptors.request.use(
    config => {

      // 在发送请求之前做些什么
      if (userConfig) {
        for(let i in userConfig) {
          config[i] = userConfig[i];
        }
      } else {
        config.timeout = 4000;
        config.headers["X-Session-Token"] = store.state.axiosHTTP.token;
        config.url = url;
        config.method = method;
      }
      setParams(config);
      return config;

    },
    error => {
      return Promise.reject(error);
  });

  // 根据不同方法设置传参
  function setParams (c) {
    switch (c.method.toLocaleUpperCase()) {
      case 'POST':
        c.data = params; // qs.stringify(params);
        break;
      // case 'DELETE':
      // case 'PUT':
      case 'GET':
        if (typeof params != 'object') {
          id = params;
          params = null;
        }
        if (id || id == 0) c.url += '/' + id;
        c.params =  params;
        break;
    }
  }

  // 响应拦截器
  axiosInstance.interceptors.response.use(
    response => {
      let code = Number(response.data.code);
      switch (code) {
        case 200:
          return response.data;
        default: 
          return response;
      }
    }, 
    error => {
      return Promise.reject(error);
  });
  
  return axiosInstance();
}

export default createAxios;