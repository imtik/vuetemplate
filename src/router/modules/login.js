const loginRouter = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/login')
  }
];

export default loginRouter;
